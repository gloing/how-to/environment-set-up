# Environment set-up

This repository is intended to be a place to store useful information for "development environment" type information for people joining the research group, including things like setting up a Python virtual environment and using VSCode to work with a Raspberry Pi.

